This service is used to keep a centralised register of invoices sent to clients.

Endpoints:  
•	Add Invoice  POST http://localhost:8080/invoices     
•	View All invoices  GET http://localhost:8080/invoices    
•	View Invoice  GET http://localhost:8080/invoices/{invoiceId}      

Run project:
 ```
  mvn clean package
  java -jar target/demo-0.0.1-SNAPSHOT.jar
```

Swagger UI:  
http://localhost:8080/swagger-ui.html