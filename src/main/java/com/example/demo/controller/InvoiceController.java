package com.example.demo.controller;

import com.example.demo.model.Invoice;
import com.example.demo.service.InvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping(value = "/invoices")
@Api(value = "Invoices")
public class InvoiceController {

    private final InvoiceService invoiceService;

    @Autowired
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @PostMapping()
    public ResponseEntity addInvoice(@RequestBody Invoice invoice, UriComponentsBuilder uriBuilder) {
        UriComponents uriComponents =
                uriBuilder.path("/invoices/{id}").buildAndExpand(invoiceService.addInvoice(invoice).getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @GetMapping()
    @ApiOperation(value = "View a list of invoices", response = List.class)
    public ResponseEntity viewAllInvoices() {
        return ResponseEntity.ok(invoiceService.getInvoices());
    }

    @GetMapping("/{invoiceId}")
    @ApiOperation(value = "View invoice by id", response = Invoice.class)
    public ResponseEntity viewInvoice(@PathVariable Long invoiceId) {
        return ResponseEntity.ok(invoiceService.getInvoiceById(invoiceId));
    }
}
