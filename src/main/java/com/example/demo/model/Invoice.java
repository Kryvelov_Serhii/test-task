package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String client;

    private Long varRate;

    private Date invoiceDate;

    @Valid
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "invoiceId")
    private List<LineItem> lineItems;

    public BigDecimal getSubTotal() {
        return lineItems
                .stream()
                .map(LineItem::getLineItemTotal)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getVal() {
        return getSubTotal().multiply(BigDecimal.valueOf(0.15)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTotal() {
        return getSubTotal().add(getVal()).setScale(2, RoundingMode.HALF_UP);
    }

}
