package com.example.demo.service;

import com.example.demo.model.Invoice;

import java.util.List;

public interface InvoiceService {

    Invoice addInvoice(Invoice invoice);

    List<Invoice> getInvoices();

    Invoice getInvoiceById(Long id);
}
